#include <QCoreApplication>
#include <zmq.h>
#include <QUuid>
#include <QDateTime>
#include <QJsonDocument>
#include <QJsonObject>
#include <cstdio>

QByteArray genJson();


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    printf("Starting...\n");
    void* ctx = zmq_ctx_new();
    if(!ctx)
    {
           fprintf(stderr, "Error occured during zmq_ctx_new(): NULL\n");
           exit(-1);
    }
    void* resp = zmq_socket(ctx, ZMQ_REP);
    if(!resp)
    {
           fprintf(stderr, "Error occured during zmq_socket(): %s\n", zmq_strerror(errno));
           exit(-1);
    }
    int rc = zmq_bind(resp, "tcp://*:9000");
    if (rc == -1)
    {
        fprintf(stderr, "Error occured during zmq_bind(): %s\n", zmq_strerror(errno));
        exit(-1);
    }

    while(true)
    {
        zmq_msg_t request;
        rc = zmq_msg_init(&request);
        if(rc == -1)
        {
            fprintf(stderr, "Error occured during zmq_msg_init(): %s\n", zmq_strerror(errno));
        }
        rc = zmq_msg_recv(&request, resp, 0);
        if(rc == -1)
        {
            fprintf(stderr, "Error occured during zmq_msg_recv(): %s\n", zmq_strerror(errno));
        }
        zmq_msg_close(&request);
        if(rc == -1)
        {
             fprintf(stderr, "Error occured during zmq_msg_close(&request): %s\n", zmq_strerror(errno));
        }


        printf("Request is received. Sending json...\n");
        QByteArray json = genJson();
        zmq_msg_t reply;
        rc = zmq_msg_init_size(&reply, json.size());
        if(rc == -1)
        {
            fprintf(stderr, "Error occured during zmq_msg_init_size(): %s\n", zmq_strerror(errno));
        }
        memcpy(zmq_msg_data(&reply), json.constData(), json.size());
        rc = zmq_msg_send(&reply, resp, 0);
        if(rc == -1)
        {
            fprintf(stderr, "Error occured during zmq_msg_send(): %s\n", zmq_strerror(errno));
        }
        rc = zmq_msg_close(&reply);
        if(rc == -1)
        {
             fprintf(stderr, "Error occured during zmq_msg_close(&reply): %s\n", zmq_strerror(errno));
        }
    }

    rc = zmq_close(resp);
    if(rc == -1)
    {
         fprintf(stderr, "Error occured during zmq_close(resp): %s\n", zmq_strerror(errno));
    }
    rc = zmq_ctx_destroy(ctx);
    if(rc == -1)
    {
         fprintf(stderr, "Error occured during zmq_ctx_destroy(&reply): %s\n", zmq_strerror(errno));
    }
    return a.exec();
}

QByteArray genJson()
{
    QString uuid =  QUuid::createUuid().toString();
    //delete "{" and "}" from uuid
    uuid.chop(1);
    uuid.remove(0,1);

    QJsonObject json;
    json["uuid"] = uuid;
    json["date"] = QDateTime::currentDateTimeUtc().toString("dddd, dd MMMM yyyy hh:mm:ss");
    return QJsonDocument(json).toJson();
}


