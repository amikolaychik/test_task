#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lcdNumber->setDigitCount(8);
    ui->label->setAlignment(Qt::AlignHCenter);
    ui->label->setText("Date");
    ui->label_2->setAlignment(Qt::AlignHCenter);
    ui->label_2->setText("offline");
}

void MainWindow::run()
{
    void *ctx = zmq_ctx_new();
    if(!ctx)
    {
           fprintf(stderr, "Error occured during zmq_ctx_new(): NULL\n");
           exit(-1);
    }
    void *req = zmq_socket(ctx, ZMQ_REQ);
    if(!req)
    {
           fprintf(stderr, "Error occured during zmq_socket(): %s\n", zmq_strerror(errno));
           exit(-1);
    }
    int rc = zmq_connect(req, "tcp://localhost:9000");
    if(rc == -1)
    {
        fprintf(stderr, "Error occured during zmq_connect(): %s\n", zmq_strerror(errno));
        exit(-1);
    }

    // create struct for zmq_poll()
    zmq_pollitem_t item[1];
    item[0].socket = req;
    item[0].events = ZMQ_POLLIN;

    int replyIsReceived = 1;

    while(true)
    {
        QApplication::processEvents();

        if(replyIsReceived)                    // if no response(reply) is received do not send a request
        {
            zmq_msg_t request;
            rc = zmq_msg_init_size(&request, 7);
            if(rc == -1)
            {
                fprintf(stderr, "Error occured during zmq_msg_init_size(): %s\n", zmq_strerror(errno));
            }
            memcpy(zmq_msg_data(&request), "getTim1", 7);
            rc = zmq_msg_send(&request, req, 0);
            if(rc == -1)
            {
                fprintf(stderr, "Error occured during zmq_msg_send(): %s\n", zmq_strerror(errno));
            }
            rc = zmq_msg_close(&request);
            if(rc == -1)
            {
                fprintf(stderr, "Error occured during zmq_msg_close(&request): %s\n", zmq_strerror(errno));
            }
        }



        rc = zmq_poll(item, 1, /*timeout=*/100);                       //check for message
        if(rc == -1)
        {
            fprintf(stderr, "Error occured during zmq_poll(): %s\n", zmq_strerror(errno));
            replyIsReceived = 0;
        }
        else if(rc == 0)                                                //no messages
        {
            ui->label_2->setText("offline");
            replyIsReceived = 0;
        }
        else                                                            //there is a message
        {
            zmq_msg_t reply;
            rc = zmq_msg_init(&reply);
            if(rc == -1)
            {
                fprintf(stderr, "Error occured during zmq_msg_init(&reply): %s\n", zmq_strerror(errno));
            }
            rc = zmq_msg_recv(&reply, req, 0);
            if(rc == -1)
            {
                fprintf(stderr, "Error occured during zmq_msg_recv(): %s\n", zmq_strerror(errno));
            }
            ui->label_2->setText("online");
            int length = zmq_msg_size(&reply);
            char *value = (char*)malloc(length+1);
            memcpy(value,zmq_msg_data(&reply),length);
            rc = zmq_msg_close(&reply);
            if(rc == -1)
            {
                 fprintf(stderr, "Error occured during zmq_msg_close(&reply): %s\n", zmq_strerror(errno));
            }
            parseJsonAndDisplay(QByteArray(value));
            free(value);
            replyIsReceived = 1;

        }
        sleep(1);
    }

    rc = zmq_close(req);
    if(rc == -1)
    {
         fprintf(stderr, "Error occured during zmq_close(req): %s\n", zmq_strerror(errno));
    }
    rc = zmq_ctx_destroy(ctx);
    if(rc == -1)
    {
         fprintf(stderr, "Error occured during zmq_ctx_destroy(): %s\n", zmq_strerror(errno));
    }

}

void MainWindow::parseJsonAndDisplay(QByteArray array)
{
    QJsonDocument jsonIn(QJsonDocument::fromJson(array));
    QJsonObject newJsonObj = jsonIn.object();
    QStringList list = newJsonObj["date"].toString().split(" ");
    ui->label->setAlignment(Qt::AlignHCenter);
    ui->label->setText(list[0]+" "+list[1]+" "+list[2]+" "+list[3]);
    ui->lcdNumber->display(list[4]);
}

MainWindow::~MainWindow()
{
    delete ui;
}
