#-------------------------------------------------
#
# Project created by QtCreator 2014-01-05T19:30:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client_req-resp
TEMPLATE = app
LIBS += -lzmq

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
