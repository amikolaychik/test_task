#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lcdNumber->setDigitCount(8);
    ui->label->setAlignment(Qt::AlignHCenter);
    ui->label->setText("Date");
    ui->label_2->setAlignment(Qt::AlignHCenter);
    ui->label_2->setText("offline");
}


void MainWindow::run()
{
    void *ctx = zmq_ctx_new();
    if(!ctx)
    {
           fprintf(stderr, "Error occured during zmq_ctx_new(): NULL\n");
           exit(-1);
    }
    void *sub = zmq_socket(ctx, ZMQ_SUB);
    if(!sub)
    {
           fprintf(stderr, "Error occured during zmq_socket(): %s\n", zmq_strerror(errno));
           exit(-1);
    }
    int rc = zmq_connect(sub, "tcp://localhost:9000");
    if(rc == -1)
    {
        fprintf(stderr, "Error occured during zmq_connect(): %s\n", zmq_strerror(errno));
        exit(-1);
    }
    rc = zmq_setsockopt(sub, ZMQ_SUBSCRIBE, 0, 0);
    if(rc == -1)
    {
        fprintf(stderr, "Error occured during zmq_setsockopt(): %s\n", zmq_strerror(errno));
        exit(-1);
    }

    // create struct for zmq_poll()
    zmq_pollitem_t item[1];
    item[0].socket = sub;
    item[0].events = ZMQ_POLLIN;

    while(true)
    {
        QApplication::processEvents();

        rc = zmq_poll(item, 1, /*timeout=*/1000);                       //check for message
        if(rc == -1)
        {
            fprintf(stderr, "Error occured during zmq_poll(): %s\n", zmq_strerror(errno));
        }
        else if(rc == 0)                                                //no messages
        {
            ui->label_2->setText("offline");
        }
        else                                                            //there is a message
        {
            zmq_msg_t msg;
            rc = zmq_msg_init(&msg);
            if(rc == -1)
            {
                fprintf(stderr, "Error occured during zmq_msg_init(): %s\n", zmq_strerror(errno));
            }
            rc = zmq_msg_recv(&msg, sub, 0);
            if(rc == -1)
            {
                fprintf(stderr, "Error occured during zmq_msg_recv(): %s\n", zmq_strerror(errno));
            }
            ui->label_2->setText("online");
            int length = zmq_msg_size(&msg);
            char *value = (char*)malloc(length+1);
            memcpy(value,zmq_msg_data(&msg),length);
            rc = zmq_msg_close(&msg);
            if(rc == -1)
            {
                 fprintf(stderr, "Error occured during zmq_msg_close(): %s\n", zmq_strerror(errno));
            }
            parseJsonAndDisplay(QByteArray(value));
            free(value);

        }
    }

    rc = zmq_close(sub);
    if(rc == -1)
    {
         fprintf(stderr, "Error occured during zmq_close(): %s\n", zmq_strerror(errno));
    }
    rc = zmq_ctx_destroy(ctx);
    if(rc == -1)
    {
         fprintf(stderr, "Error occured during zmq_ctx_destroy(): %s\n", zmq_strerror(errno));
    }

}

void MainWindow::parseJsonAndDisplay(QByteArray array)
{
    QJsonDocument jsonIn(QJsonDocument::fromJson(array));
    QJsonObject newJsonObj = jsonIn.object();
    QStringList list = newJsonObj["date"].toString().split(" ");
    ui->label->setAlignment(Qt::AlignHCenter);
    ui->label->setText(list[0]+" "+list[1]+" "+list[2]+" "+list[3]);
    ui->lcdNumber->display(list[4]);
}


MainWindow::~MainWindow()
{
    delete ui;
}

