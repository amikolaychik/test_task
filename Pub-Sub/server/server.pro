#-------------------------------------------------
#
# Project created by QtCreator 2014-01-03T23:20:00
#
#-------------------------------------------------

QT       += core

QT       -= gui
LIBS += -lzmq
TARGET = zmqserv
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
