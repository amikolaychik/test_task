#include <QCoreApplication>
#include <QUuid>
#include <QDateTime>
#include <QJsonDocument>
#include <QJsonObject>
#include <zmq.h>
#include <cstdio>

QByteArray genJson();

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    printf("Starting...\n");
    //create new context
    void *ctx = zmq_ctx_new();
    if(!ctx)
    {
           fprintf(stderr, "Error occured during zmq_ctx_new(): NULL\n");
           exit(-1);
    }
    //create socket
    void *pub = zmq_socket(ctx, ZMQ_PUB);
    if(!pub)
    {
           fprintf(stderr, "Error occured during zmq_socket(): %s\n", zmq_strerror(errno));
           exit(-1);
    }

    int rc = zmq_bind(pub, "tcp://*:9000");
    if (rc == -1)
    {
        fprintf(stderr, "Error occured during zmq_bind(): %s\n", zmq_strerror(errno));
        exit(-1);
    }


    printf("Publishing\n");
    while(true)
    {
	
        QByteArray json = genJson();
        zmq_msg_t msg;
        rc = zmq_msg_init_size(&msg, json.size());
        if(rc == -1)
        {
             fprintf(stderr, "Error occured during zmq_msg_init_size(): %s\n", zmq_strerror(errno));
        }
        memcpy(zmq_msg_data(&msg), json.constData(), json.size());
        rc = zmq_msg_send(&msg, pub, 0);
        if(rc == -1)
        {
             fprintf(stderr, "Error occured during zmq_msg_init_size(): %s\n", zmq_strerror(errno));
        }
        rc = zmq_msg_close(&msg);
        if(rc == -1)
        {
             fprintf(stderr, "Error occured during zmq_msg_close(): %s\n", zmq_strerror(errno));
        }
        sleep(1);

    }
    rc = zmq_close(pub);
    if(rc == -1)
    {
         fprintf(stderr, "Error occured during zmq_close(): %s\n", zmq_strerror(errno));
    }
    zmq_ctx_destroy(ctx);
    if(rc == -1)
    {
         fprintf(stderr, "Error occured during zmq_ctx_destroy(): %s\n", zmq_strerror(errno));
    }

    return a.exec();
}

QByteArray genJson()
{
    QString uuid =  QUuid::createUuid().toString();
    //delete "{" and "}" from uuid
    uuid.chop(1);
    uuid.remove(0,1);

    QJsonObject json;
    json["uuid"] = uuid;
    json["date"] = QDateTime::currentDateTimeUtc().toString("dddd, dd MMMM yyyy hh:mm:ss");
    return QJsonDocument(json).toJson();
}
